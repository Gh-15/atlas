package com.example.amalzoheir.atlasapp;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.TextView;
import android.widget.VideoView;

import java.util.List;

public class TextInContentAdapter extends RecyclerView.Adapter<TextInContentAdapter.TextInContentViewHolder>{
    private List<TextAtlasInContent> textAtlasInContentList;
    public class TextInContentViewHolder extends RecyclerView.ViewHolder {
        public TextView dataInContentTextView;
        public ImageView imageContentView;
        public VideoView contentVideoView;
        private static final int FOOTER_VIEW = 1;
        public TextInContentViewHolder(View view) {
            super(view);
            dataInContentTextView=(TextView)view.findViewById(R.id.text_content_tv);
            imageContentView=(ImageView)view.findViewById(R.id.image_content_picture);
        }
    }

    @Override
    public int getItemCount() {
        return textAtlasInContentList.size();
    }

    @Override
    public TextInContentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View contentItemView= LayoutInflater.from(parent.getContext()).inflate(R.layout.list_items_for_content,parent,false);
        return new TextInContentAdapter.TextInContentViewHolder(contentItemView);
    }

    @Override
    public void onBindViewHolder(TextInContentViewHolder holder, int position) {
        TextAtlasInContent currentTextAtlasInContent=textAtlasInContentList.get(position);
        holder.dataInContentTextView.setText(currentTextAtlasInContent.getDataAboutContent());
        if(currentTextAtlasInContent.hasImage()){
            holder.imageContentView.setImageResource(currentTextAtlasInContent.getImageSourceNumber());
            holder.imageContentView.setVisibility(View.VISIBLE);
        }
        else{
         holder.imageContentView.setVisibility(View.GONE);
        }


    }

    public TextInContentAdapter(List<TextAtlasInContent> textAtlasInContentList){
        this.textAtlasInContentList=textAtlasInContentList;
    }
    private class VIEW_TYPES {
        public static final int Header = 1;
        public static final int Normal = 2;
        public static final int Footer = 3;
    }
}

