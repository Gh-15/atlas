package com.example.amalzoheir.atlasapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Date;

public class ResultsActivity extends AppCompatActivity {
    TextView correct_answers, wrong_answers, percent, exam_time, exam_date;
    Button finish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_results);
        getSupportActionBar().setTitle(R.string.results_title);
        correct_answers = findViewById(R.id.correct_answers_num);
        wrong_answers = findViewById(R.id.wrong_answers_num);
        percent = findViewById(R.id.percent);
        exam_time = findViewById(R.id.exam_time);
        exam_date = findViewById(R.id.exam_date);
        finish = findViewById(R.id.finish);
        if (getIntent().getExtras() != null) {
            int correct = getIntent().getExtras().getInt("correct");
//            double correct = Double.parseDouble(correctAns);
            correct_answers.setText(correct+"");
            wrong_answers.setText((80 - correct) + "");
            percent.setText(((correct / 80.0) * 100.0) + "%");
        }
        Date today = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss a");
        String dateToStr = format.format(today);
        exam_date.setText(dateToStr.substring(0, dateToStr.indexOf(' ')));
        exam_time.setText(dateToStr.substring(dateToStr.indexOf(' ') + 1));
        finish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }
}
