package com.example.amalzoheir.atlasapp;

public class TextAtlasInContent {
    private static final int NO_IMAGE_PROVIDED=-1;
    private static final int NO_VIDEO_PROVIDED=-1;
    private static final int NO_Question_PROVIDED=-1;
    private String dataAboutContent;
    private String answerMCQ1;
    private String answerMCQ2;
    private String answerMCQ3;
    private String answerMCQ4;
    private String correctAnswer;
    private boolean hasAudio;
    private boolean isTrueAndFalseQuestion=false;
    private int imageSourceNumber=NO_IMAGE_PROVIDED;
    private int videoSourceNumber=NO_VIDEO_PROVIDED;
    private int questionProvided=NO_Question_PROVIDED;
    public TextAtlasInContent(String dataAboutContent){
        this.dataAboutContent=dataAboutContent;
    }
    public TextAtlasInContent(String dataAboutContent,boolean hasAudio){
        this.dataAboutContent=dataAboutContent;
        this.hasAudio=hasAudio;
    }
    public TextAtlasInContent(String dataAboutContent,int questionProvided,
                              String answerMCQ1,String answerMCQ2,String correctAnswer) {
        isTrueAndFalseQuestion=true;
        this.dataAboutContent=dataAboutContent;
        this.questionProvided=questionProvided;
        this.answerMCQ1=answerMCQ1;
        this.answerMCQ2=answerMCQ2;
        this.correctAnswer=correctAnswer;
    }
    public TextAtlasInContent(String dataAboutContent,int questionProvided,
                              String answerMCQ1,String answerMCQ2,
                              String answerMCQ3,String answerMCQ4,String correctAnswer){
        this.dataAboutContent=dataAboutContent;
        this.questionProvided=questionProvided;
        this.answerMCQ1=answerMCQ1;
        this.answerMCQ2=answerMCQ2;
        this.answerMCQ3=answerMCQ3;
        this.answerMCQ4=answerMCQ4;
        this.correctAnswer=correctAnswer;

    }
//    public TextAtlasInContent(int imageSourceNumber, int imageSourceNumber)
    public TextAtlasInContent(String dataAboutContent,int imageSourceNumber){
        this.dataAboutContent=dataAboutContent;
        this.imageSourceNumber=imageSourceNumber;
    }

    public TextAtlasInContent(String dataAboutContent,int videoSourceNumber,int imageSourceNumber) {
        this.dataAboutContent = dataAboutContent;
        this.videoSourceNumber = videoSourceNumber;
        this.imageSourceNumber = NO_IMAGE_PROVIDED;
    }

    public  int getNoImageProvided() {
        return imageSourceNumber;
    }

    public int getImageSourceNumber() {
        return imageSourceNumber;
    }

    public int getVideoSourceNumber() {
        return videoSourceNumber;
    }

    public String getDataAboutContent() {
        return dataAboutContent;
    }

    public void setDataAboutContent(String dataAboutContent) {
        this.dataAboutContent = dataAboutContent;
    }

    public void setImageSourceNumber(int imageSourceNumber) {
        this.imageSourceNumber = imageSourceNumber;
    }

    public void setVideoSourceNumber(int videoSourceNumber) {
        this.videoSourceNumber = videoSourceNumber;
    }

    public String getAnswerMCQ1() {
        return answerMCQ1;
    }

    public String getAnswerMCQ2() {
        return answerMCQ2;
    }

    public String getAnswerMCQ3() {
        return answerMCQ3;
    }

    public String getAnswerMCQ4() {
        return answerMCQ4;
    }

    public String getCorrectAnswer() {
        return correctAnswer;
    }
public boolean isHasAudio(){
        return hasAudio;
}
    public boolean hasImage() {
        return imageSourceNumber != NO_IMAGE_PROVIDED;
    }
    public boolean hasVideo() {
        return videoSourceNumber != NO_VIDEO_PROVIDED;
    }
    public boolean isQuestion() {
        return questionProvided !=NO_Question_PROVIDED;
    }
    public boolean isTrueAndFalseQuestionChecked(){
        return isTrueAndFalseQuestion;
    }
    public int getQuestionProvided() {
        return questionProvided;
    }
}
