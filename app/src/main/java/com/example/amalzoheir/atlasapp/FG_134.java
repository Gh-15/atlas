package com.example.amalzoheir.atlasapp;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.amalzoheir.atlasapp.home.FourteenFragment;
import com.example.amalzoheir.atlasapp.home.TwelveFragment;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FG_134 extends Fragment {
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos;
    RadioGroup radiogroup1, radiogroup2, radiogroup3, radiogroup4, radioGroup6, radioGroup5, radioGroup7;
    int succees = 0;
    double fail = 0.0;
    MainPage activity;

    public FG_134() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fg_134, container, false);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);


        radiogroup4 = rootView.findViewById(R.id.radioGroup4);
        activity = (MainPage) getActivity();


        radiogroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton5) {
                    succees = 1;
                } else succees = 0;
                activity.setCorrectAnswers("q49", succees);
                activity.saveIntoSharedPrefrences("q49", checkedId);

            }
        });
        if (activity.getFromSharedPrefrence("q49") != 0)
            radiogroup4.check(activity.getFromSharedPrefrence("q49"));



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_135());

            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_133());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });
        return rootView;
    }

}
