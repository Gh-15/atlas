package com.example.amalzoheir.atlasapp;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.amalzoheir.atlasapp.answers.AnswerMCQ;

public class AnswerMSQTrueAndFalse extends AppCompatActivity {
    private String questionText;
    private String answer1;
    private String answer2;
    private String correctAnswer;
    TextView questionTextView;
    RadioGroup containerRadioGroup;
    RadioButton answer1RadioButton;
    RadioButton answer2RadioButton;
    Button confirmAnserButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer_msqtrue_and_false);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            questionText =extras.getString("question");
            answer1 =extras.getString("answer1");
            answer2 =extras.getString("answer2");
            correctAnswer=extras.getString("correctAnswer");
            questionTextView=(TextView)findViewById(R.id.questionTVTrueAndFalse);
            questionTextView.setText(questionText);
            containerRadioGroup=(RadioGroup)findViewById(R.id.container_radio_groupTrueAndFalse);
            answer1RadioButton=(RadioButton)findViewById(R.id.answer_1_rbTrueAndFalse);
            answer1RadioButton.setText(answer1);
            answer2RadioButton=(RadioButton)findViewById(R.id.answer_2_rbTrueAndFalse);
            answer2RadioButton.setText(answer2);
            confirmAnserButton=(Button)findViewById(R.id.confirm_answer_buttonTrueAndFalse);
            final MediaPlayer correctanserMediaplayer=MediaPlayer.create(AnswerMSQTrueAndFalse.this,R.raw.correct);
            final MediaPlayer notCorrectanserMediaplayer=MediaPlayer.create(AnswerMSQTrueAndFalse.this,R.raw.correct);
            confirmAnserButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String value =
                            ((RadioButton)findViewById(containerRadioGroup.getCheckedRadioButtonId()))
                                    .getText().toString();
                    if(value.equals(correctAnswer)) {
                        Toast.makeText(AnswerMSQTrueAndFalse.this, "اجابه صحيحه" , Toast.LENGTH_LONG).show();
                        correctanserMediaplayer.start();
                    }
                    else{
                        Toast.makeText(AnswerMSQTrueAndFalse.this, "اجابه خاطئه", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }
    }
}
