package com.example.amalzoheir.atlasapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import java.util.ArrayList;
public class FG_42 extends Fragment {
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos, succees = 0;
    MainPage activity;
    RadioGroup radiogroup1, radiogroup2, radiogroup3, radiogroup4;
    public String succes_string, fail_string, new_succes_string, new_fail_string = "";
    double fail = 0.0;


    public FG_42() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fg_42, container, false);
        //recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_fourth);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        RecyclerView.LayoutManager textLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        //recyclerView.setLayoutManager(textLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setAdapter(textInSectionAdapter);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);
        //radiogroup1 = rootView.findViewById(R.id.radioGroup1);
        radiogroup2 = rootView.findViewById(R.id.radioGroup2);
//        radiogroup3 = rootView.findViewById(R.id.radioGroup3);
//        radiogroup4 = rootView.findViewById(R.id.radioGroup4);
        activity = (MainPage) getActivity();

        radiogroup2.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton8) {
                    succees = 1;
                } else succees = 0;
                activity.setCorrectAnswers("q10", succees);
                activity.saveIntoSharedPrefrences("q10", checkedId);

            }
        });
        if (activity.getFromSharedPrefrence("q10") != 0)
            radiogroup2.check(activity.getFromSharedPrefrence("q10"));





        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_43());
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new fourthFragment());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });

        return rootView;


    }

}
