
package com.example.amalzoheir.atlasapp;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class intoductionFragment extends Fragment {
    private static FragmentManager fragmentManager;
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView textView;

    private TextInSectionAdapter textInSectionAdapter;
    private ViewPager viewPager;
    private static int pos;
    private int inc;
    private int dec;
    MainPage activity;

    public intoductionFragment(ViewPager viewPager, int position, int inc, int dec) {
        this.viewPager = viewPager;
        this.pos = position;
        this.inc = inc;
        this.dec = dec;
        Log.e("pos", String.valueOf(pos));
        Log.e("inc", String.valueOf(inc));
        Log.e("dec", String.valueOf(dec));

    }

    public intoductionFragment() {

    }

    private Button btnHome, btnNext, btnExit, btnPrevious;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.introduction_fragment_layout, container, false);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        textView = (TextView) rootView.findViewById(R.id.Intro);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        activity = (MainPage) getActivity();

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new mindMapsFragment());
            }
        });

        prepareDataInTextInSections();
        return rootView;


    }


    private void prepareDataInTextInSections() {
        textAtlasInSectionsArrayList.clear();
    }
}

