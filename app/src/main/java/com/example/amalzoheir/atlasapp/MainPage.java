package com.example.amalzoheir.atlasapp;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.HashMap;
import java.util.Map;

public class
MainPage extends AppCompatActivity {


    private int correrct = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_page);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setFragment(new intoductionFragment());
        deleteSharedPrefrenceFile(false);
    }


    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        public PlaceholderFragment() {
        }

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main_page, container, false);
            TextView textView = (TextView) rootView.findViewById(R.id.section_label);
            textView.setText(getString(R.string.section_format, getArguments().getInt(ARG_SECTION_NUMBER)));
            return rootView;
        }
    }

    HashMap<String, Integer> answers = new HashMap<>();

    public void setCorrectAnswers(String key, int correctAnswers) {
        answers.put(key, correctAnswers);
    }

    public void openResultsActivity() {
        deleteSharedPrefrenceFile(true);
    }

    public void setFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction().replace(R.id.container, fragment).addToBackStack(null).commit();
    }

    public void saveIntoSharedPrefrences(final String qn, final int rn) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainPage.this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(qn, rn).apply();
        Log.e("saved", preferences.getInt(qn, 0) + "");
    }

    public int getFromSharedPrefrence(String qn) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainPage.this);
        return preferences.getInt(qn, 0);
    }

    public void deleteSharedPrefrenceFile(final boolean open) {
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... voids) {
                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(MainPage.this);
                preferences.edit().clear().commit();
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                if (open) {
                    for (Map.Entry<String, Integer> entry : answers.entrySet()) {
                        String key = entry.getKey();
                        Integer value = entry.getValue();
                        correrct += value;
                    }
                    startActivity(new Intent(MainPage.this, ResultsActivity.class).putExtra("correct", correrct));
                    finish();
                }
            }
        };
        task.execute();
    }
}

