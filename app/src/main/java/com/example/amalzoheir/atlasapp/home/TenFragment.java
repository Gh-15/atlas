package com.example.amalzoheir.atlasapp.home;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.amalzoheir.atlasapp.FG_102;
import com.example.amalzoheir.atlasapp.FG_92;
import com.example.amalzoheir.atlasapp.HeaderDecoration;
import com.example.amalzoheir.atlasapp.MainPage;
import com.example.amalzoheir.atlasapp.R;
import com.example.amalzoheir.atlasapp.TextAtlasInSections;
import com.example.amalzoheir.atlasapp.TextInSectionAdapter;
import com.example.amalzoheir.atlasapp.fifthFragment;
import com.example.amalzoheir.atlasapp.intoductionFragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class TenFragment extends Fragment {
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos;
    RadioGroup radiogroup, radiogroup2, radiogroup3, radiogroup4;
    int succees = 0;
    double fail = 0.0;
    MainPage activity;

    public TenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_ten, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_first);

        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        RecyclerView.LayoutManager textLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(textLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(textInSectionAdapter);
        HeaderDecoration hr = new HeaderDecoration(getContext(), recyclerView, R.layout.button);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);
        activity = (MainPage) getActivity();



        //radiogroup = rootView.findViewById(R.id.radioGroup);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new ElevenFragment());
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_92());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });
        prepareDataInTextInSections();
        return rootView;
    }

    private void prepareDataInTextInSections() {
        textAtlasInSectionsArrayList.clear();
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain184), R.mipmap.greenicon, 1));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain185), R.mipmap.yellowicon, 2));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain186), R.mipmap.blueicon, 3));
    }
}
