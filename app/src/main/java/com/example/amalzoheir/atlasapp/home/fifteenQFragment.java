package com.example.amalzoheir.atlasapp.home;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.amalzoheir.atlasapp.FG_52;
import com.example.amalzoheir.atlasapp.FG_53;
import com.example.amalzoheir.atlasapp.MainPage;
import com.example.amalzoheir.atlasapp.R;
import com.example.amalzoheir.atlasapp.TextAtlasInSections;
import com.example.amalzoheir.atlasapp.TextInSectionAdapter;
import com.example.amalzoheir.atlasapp.intoductionFragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link fifteenQFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link fifteenQFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class fifteenQFragment extends Fragment {
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos;
    MainPage activity;
    MediaPlayer audioForTopicTwo;
    RadioGroup radiogroup1, radiogroup2, radiogroup3, radiogroup4;
    int succees = 0;
    double fail = 0.0;


    public fifteenQFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fifteen_q, container, false);
        //recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_fifth);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        RecyclerView.LayoutManager textLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        //recyclerView.setLayoutManager(textLayoutManager);
        //recyclerView.setItemAnimator(new DefaultItemAnimator());
        //recyclerView.setAdapter(textInSectionAdapter);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);
        radiogroup3 = rootView.findViewById(R.id.radioGroup3);
        activity = (MainPage) getActivity();

        radiogroup3.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton9) {
                    succees = 1;
                } else succees = 0;
                activity.setCorrectAnswers("q15", succees);
                activity.saveIntoSharedPrefrences("q15", checkedId);

            }
        });
        if (activity.getFromSharedPrefrence("q15") != 0)
            radiogroup4.check(activity.getFromSharedPrefrence("q15"));


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_53());
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_52());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });

        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });

//        prepareDataInTextInSections();
        audioForTopicTwo = MediaPlayer.create(this.getContext().getApplicationContext(), R.raw.six);
        //audioForTopicTwo.start();

        return rootView;
    }
}
