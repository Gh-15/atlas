package com.example.amalzoheir.atlasapp;

import android.media.MediaPlayer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class AudioActivity extends AppCompatActivity {
    Button play_button;
    Button stop_button;
    private int audioID;
    public MediaPlayer mp;
    @Override
    protected void onStop() {
        super.onStop();
        mp.stop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
          mp.stop();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_audio);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            audioID = extras.getInt("audioID");

        }
        mp = MediaPlayer.create(this, audioID);
        play_button = (Button)this.findViewById(R.id.button_play_audio);
        play_button.setOnClickListener(
                new View.OnClickListener() {
                    public void onClick(View v) {
                        mp.start();

                    }
                });
        stop_button = (Button)this.findViewById(R.id.button_stop_audio);
        stop_button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mp.stop();
            }
        });

    }
}
