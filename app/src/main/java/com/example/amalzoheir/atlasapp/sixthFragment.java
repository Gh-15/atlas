package com.example.amalzoheir.atlasapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.amalzoheir.atlasapp.home.SevenFragment;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class sixthFragment extends Fragment {
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos;
    RadioGroup radiogroup1, radiogroup2, radiogroup3, radiogroup4;
    int succees = 0;
    double fail = 0.0;
    MainPage activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.sixth_section_fragment_layout, container, false);
        //recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_sixth);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        RecyclerView.LayoutManager textLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        //recyclerView.setLayoutManager(textLayoutManager);
        // recyclerView.setItemAnimator(new DefaultItemAnimator());
        // recyclerView.setAdapter(textInSectionAdapter);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);


        radiogroup1 = rootView.findViewById(R.id.radioGroup1);
        radiogroup2 = rootView.findViewById(R.id.radioGroup2);
        radiogroup3 = rootView.findViewById(R.id.radioGroup3);
        radiogroup4 = rootView.findViewById(R.id.radioGroup4);
        activity = (MainPage) getActivity();
        radiogroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton3) {
                    succees = 1;
                } else succees = 0;
                activity.setCorrectAnswers("q17", succees);
                activity.saveIntoSharedPrefrences("q17", checkedId);

            }
        });
        if (activity.getFromSharedPrefrence("q17") != 0)
            radiogroup1.check(activity.getFromSharedPrefrence("q17"));


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_62());
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_53());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });
        /*
        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(), recyclerView, new RecyclerTouchListener.ClickListener() {
            @Override
            public void onClick(View view, int position) {
                TextAtlasInSections textAtlasInSections = textAtlasInSectionsArrayList.get(position);
                    Intent intentContent = new Intent(getActivity(), SixthGoal.class);
                    intentContent.putExtra("position", textAtlasInSections.getPosition());
                    startActivity(intentContent);

            }

            @Override
            public void onLongClick(View view, int position) {

            }
        }));
        */
        prepareDataInTextInSections();
        return rootView;
    }

    private void prepareDataInTextInSections() {
        textAtlasInSectionsArrayList.clear();
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain1), R.mipmap.greenicon, 1));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.introduction), R.mipmap.yellowicon, 2));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.SixthTopicWriteCode), R.mipmap.blueicon, 3));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.SixthTopicMakeExe), R.mipmap.blueicon, 4));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.remember), R.mipmap.blueicon, 5));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.activities), R.mipmap.greenicon, 6));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.final_homework), R.mipmap.redicon, 7));
        //textInSectionAdapter.notifyDataSetChanged();
    }
}

