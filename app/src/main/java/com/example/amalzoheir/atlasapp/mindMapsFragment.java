package com.example.amalzoheir.atlasapp;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import java.util.ArrayList;

@SuppressLint("ValidFragment")
public class mindMapsFragment extends Fragment {
    private static FragmentManager fragmentManager;
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private static int pos;
    private int inc;
    private int dec;
    MainPage activity;

    public mindMapsFragment() {

    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.mind_map_section_fragment_layout, container, false);
        recyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view_map);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        RecyclerView.LayoutManager textLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recyclerView.setLayoutManager(textLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(textInSectionAdapter);
        activity = (MainPage) getActivity();
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new firstFragment());
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });

        prepareDataInTextInSections();
        return rootView;
    }


    private void prepareDataInTextInSections() {
        textAtlasInSectionsArrayList.clear();
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain7), R.mipmap.redicon, 1));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain8), R.mipmap.greenicon, 2));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain9), R.mipmap.greenicon, 3));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain10), R.mipmap.greenicon, 4));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain11), R.mipmap.greenicon, 5));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain12), R.mipmap.greenicon, 6));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain13), R.mipmap.greenicon, 7));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain14), R.mipmap.greenicon, 8));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain15), R.mipmap.greenicon, 9));
        textAtlasInSectionsArrayList.add(new TextAtlasInSections(getString(R.string.main_domain16), R.mipmap.greenicon, 10));
        //textInSectionAdapter.notifyDataSetChanged();
    }
}

