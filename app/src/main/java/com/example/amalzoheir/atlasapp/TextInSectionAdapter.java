package com.example.amalzoheir.atlasapp;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class TextInSectionAdapter extends RecyclerView.Adapter<TextInSectionAdapter.TextInSectionViewHolder>{
    private List<TextAtlasInSections> textAtlasInSectionsList;
    @Override
    public TextInSectionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if (viewType == R.layout.list_item) {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        } else {
            itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.button, parent, false);
        }

        return new TextInSectionViewHolder(itemView);
    }

    public TextInSectionAdapter(List<TextAtlasInSections> textAtlasInSectionsList) {
        this.textAtlasInSectionsList = textAtlasInSectionsList;
    }

    @Override
    public int getItemViewType(int position) {
        return (position == textAtlasInSectionsList.size()) ? R.layout.button : R.layout.list_item;
    }

    @Override
    public void onBindViewHolder(final TextInSectionViewHolder holder, int position) {
        if (position == textAtlasInSectionsList.size()) {
            holder.button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(view.getContext(), "Button Clicked", Toast.LENGTH_LONG).show();
                }
            });
        }
        TextAtlasInSections currentTextAtlasInSections=textAtlasInSectionsList.get(position);
        holder.dataInSectionTextView.setText(currentTextAtlasInSections.getDataAboutSection());
        if(currentTextAtlasInSections.hasImage()){
            holder.iconImageView.setImageResource(currentTextAtlasInSections.getImageSourceNumber());
            holder.iconImageView.setVisibility(View.VISIBLE);
        }
        else
        {
            holder.iconImageView.setVisibility(View.GONE);
        }

    }

    public class TextInSectionViewHolder extends RecyclerView.ViewHolder {
        public TextView dataInSectionTextView;
        public ImageView iconImageView;
        public Button button;

        public TextInSectionViewHolder(View view) {
            super(view);
            dataInSectionTextView = (TextView) view.findViewById(R.id.data_in_section_tv);
            iconImageView = (ImageView) view.findViewById(R.id.icon_image_view);
            button = (Button) view.findViewById(R.id.buttonNext);
        }
    }

    @Override
    public int getItemCount() {
        return textAtlasInSectionsList.size();
    }
}