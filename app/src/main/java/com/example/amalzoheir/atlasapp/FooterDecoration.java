package com.example.amalzoheir.atlasapp;

import android.support.v7.widget.RecyclerView;
import android.view.View;

public class FooterDecoration extends RecyclerView.ViewHolder {
    public FooterDecoration(View itemView) {
        super(itemView);
        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do whatever you want on clicking the item
            }
        });
    }
}