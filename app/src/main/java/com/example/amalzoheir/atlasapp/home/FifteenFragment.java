package com.example.amalzoheir.atlasapp.home;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import com.example.amalzoheir.atlasapp.FG_147;
import com.example.amalzoheir.atlasapp.FG_152;
import com.example.amalzoheir.atlasapp.MainPage;
import com.example.amalzoheir.atlasapp.R;
import com.example.amalzoheir.atlasapp.TextAtlasInSections;
import com.example.amalzoheir.atlasapp.TextInSectionAdapter;
import com.example.amalzoheir.atlasapp.intoductionFragment;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class FifteenFragment extends Fragment {
    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private TextInSectionAdapter textInSectionAdapter;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos;
    RadioGroup radiogroup1, radiogroup2, radiogroup3, radiogroup4, radioGroup6, radioGroup5, radioGroup7;
    int succees = 0;
    double fail = 0.0;
    MainPage activity;


    public FifteenFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_fifteen, container, false);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);


        radiogroup1 = rootView.findViewById(R.id.radioGroup);
        radiogroup2 = rootView.findViewById(R.id.radioGroup2);
        radiogroup3 = rootView.findViewById(R.id.radioGroup3);
        radiogroup4 = rootView.findViewById(R.id.radioGroup4);
        radioGroup6 = rootView.findViewById(R.id.radioGroup6);
        radioGroup5 = rootView.findViewById(R.id.radioGroup5);
        radioGroup7 = rootView.findViewById(R.id.radioGroup7);

        activity = (MainPage) getActivity();
        radiogroup1.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton00) {
                    succees = 1;
                } else succees = 0;
                activity.setCorrectAnswers("q60", succees);
                activity.saveIntoSharedPrefrences("q60", checkedId);

            }
        });
        if (activity.getFromSharedPrefrence("q60") != 0)
            radiogroup1.check(activity.getFromSharedPrefrence("q60"));


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_152());

            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_147());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });
        return rootView;
    }

}
