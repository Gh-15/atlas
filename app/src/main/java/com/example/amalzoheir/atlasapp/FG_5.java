package com.example.amalzoheir.atlasapp;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RadioGroup;

import java.util.ArrayList;

public class FG_5 extends Fragment {

    private ArrayList<TextAtlasInSections> textAtlasInSectionsArrayList = new ArrayList<>();
    private RecyclerView recyclerView;
    RadioGroup radiogroup1, radiogroup2, radiogroup3, radiogroup4;
    int succees = 0;
    int fail = 0;
    private TextInSectionAdapter textInSectionAdapter;
    MediaPlayer audioForTopicTwo;
    private Button btnHome, btnNext, btnExit, btnPrevious;
    private ViewPager viewPager;
    private int pos;
    private int inc;
    private int dec;
    MainPage activity;
    SharedPreferences sharedpreferences;
    public static final String MyPREFERENCES = "MyPrefs";

    public String succes_string, fail_string, new_succes_string, new_fail_string = "";


    secondFragment.DataPassListener mCallback;

    public interface DataPassListener {
        public void passData(String data);
    }

    public FG_5() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fg_5, container, false);
        textInSectionAdapter = new TextInSectionAdapter(textAtlasInSectionsArrayList);
        RecyclerView.LayoutManager textLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        btnNext = rootView.findViewById(R.id.btnNext);
        btnHome = rootView.findViewById(R.id.btnHome);
        btnPrevious = rootView.findViewById(R.id.btnPrevious);
        btnExit = rootView.findViewById(R.id.btnExit);
//        radiogroup1 = rootView.findViewById(R.id.radioGroup1);
//        radiogroup2 = rootView.findViewById(R.id.radioGroup1);
//        radiogroup3 = rootView.findViewById(R.id.radioGroup1);
        radiogroup4 = rootView.findViewById(R.id.radioGroup4);

        activity = (MainPage) getActivity();

        radiogroup4.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.radioButton13) {
                    succees = 1;
                } else succees = 0;
                activity.setCorrectAnswers("q4", succees);
                activity.saveIntoSharedPrefrences("q4", checkedId);

            }
        });
        if (activity.getFromSharedPrefrence("q4") != 0)
            radiogroup4.check(activity.getFromSharedPrefrence("q4"));


        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new thirdFragment());
            }
        });
        btnPrevious.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new FG_4());
            }
        });
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.setFragment(new intoductionFragment());
            }
        });
        btnExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(getActivity());

                exitDialog.setTitle("Warning");
                exitDialog.setMessage("Exit Program");
                exitDialog.setPositiveButton("YSE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {
                        getActivity().finish();
                    }
                });
                exitDialog.setNegativeButton("NO", null);
                exitDialog.show();
            }


        });
        audioForTopicTwo = MediaPlayer.create(this.getContext().getApplicationContext(), R.raw.six);
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        audioForTopicTwo.stop();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        audioForTopicTwo.stop();

    }
}
